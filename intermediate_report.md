# CS244 Final Project - Evaluating PCC: Re-architecting Congestion Control for High Performance

## Introduction

In the PCC paper, Monte (Mo) Dong, et. al. argue that TCP and its variants can’t
consistently provide high performance in changing network conditions. To handle
complex network behavior, TCP relies on **hardwired mappings**—that is, fixed
responses (like halving cwnd) for packet-level events (like drops and RTT
increases). In doing so, TCP implicitly makes faulty assumptions about the
causes of and proper responses to network unknowns. This causes TCP and its
variants to produce suboptimal throughput and congestion control behavior.

With PCC, the client runs A/B tests to determine what rate it should send at. It
sends data at multiple rates as experiments, and uses observed network behavior
as input to a utility function that expresses desired network performance
characteristics. It then chooses what rates to send at to produce the best
output utility.

This model doesn’t make assumptions about network behavior, so the authors claim
it can handle complex, varied network conditions flexibly. With the right
utility function for fairness and convergence, they argue that PCC consistently
produces 10x faster performance than TCP, and doesn’t require hardware support
or new packet formats, making it relatively easy to adopt.

## Motivation

PCC is an interesting protocol to analyze because it claims to produce
dramatically higher throughput than TCP under the varied network conditions that
compose modern links today. TCP, for instance, penalizes flows that have long
RTTs because it takes longer for the congestion control algorithm to observe
packet losses and transmissions, forcing it to slow down the entire TCP cycle.
Additionally, TCP penalizes links with high packet loss rates by causing cwnd to
enter congestion avoidance more frequently than those with lower packet loss
rates. In both cases the authors argue that PCC exhibits far more efficient use
of network equipment and offers greater fairness for servicing different flows
over the network that experience complex, different network conditions.

The conditions above are not farfetched scenarios these days. Long RTTs
correspond to satellite link connections and packets destined across the globe,
tunneling through multiple networks; high packet loss scenarios happen all the
time in wireless networks that experience significant interference, and mobile
networks. In other words, the conditions PCC claims to dramatically increase
throughput in correspondence to ubiquitous scenarios in modern networks.

Additionally, if PCC's performance guarantees are reproducible and theoretically
valid, then it begs the question about the way that routers and clients on the
internet ought to behave. PCC was designed to not require changes to internet
infrastructure, but it doesn't behave kindly to existing TCP flows, since it
uses a completely different congestion control model than TCP. A PCC client can
easily saturate and harm the quality of service for TCP flows, so it begs the
question of whether or not the internet's reliance on TCP for the reliable
transfer of data has been a tussle in the development of optimal data flow
algorithms. We are interested in exploring the ramifications of such a finding.

## Goals

We intend to replicate two of the figures: Figure 7 and Figure 9. Figure 7 shows
that in scenarios with high packet loss rates, PCC maintains about 10x the
throughput of TCP. Figure 9 demonstrates that in shallow-buffered networks, PCC
reaches optimal throughput with even miniscule buffer sizes, while TCP performs
poorly until buffer sizes reach an orders of magnitude greater than those PCC
requires. If we have the time after reproducing these figures, the PCC paper
contains several other scenarios we may attempt to reproduce, including Figure 6
(emulated satellite links).

## Methodology

The authors of PCC provide the source code for their implementation of the PCC
protocol that transmits over UDP. With some code changes to make it easier to
collect data about the performance of PCC flows, we have compiled it into
binaries and will compare its runtime behavior versus that of TCP on network
topologies generated by Mininet. We have created an Amazon AMI with all the
configuration necessary to to run the experiments in c3.xlarge Amazon EC2 boxes.
This will make it easier for others to reproduce the results we find during this
project.

## Progress and Plans (TODO: remove)

In addition to the progress described in the above sections, we are in the
process of reproducing Figure 7. We are currently fixing issues regarding mixing
PCC with a VM since PCC's threads need to be pinned to particular cores. We have
already replicated the TCP results for Figure 7 and once we replicate the
results for PCC, we hope to focus on reproducing Figure 9. Then, we will graph
our data and discuss what we saw in the various scenarios and how relevant they
were to the claims made in the paper.

## Results (TODO: add)

## Conclusion (TODO: add)
