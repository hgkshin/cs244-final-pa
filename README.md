# CS244 Final Project - Evaluating PCC: Re-architecting Congestion Control for High Performance

## Instructions to run
### Note: all of the VPC/AMI/EC2 instances must be in us-west-2 of AWS (Oregon)

### Create a VPC (virtual private cloud). This will allow you to start a dedicated AWS instance.
    1. Go to the VPC (virtual private cloud) console from the AWS management console (quick link below).
       https://console.aws.amazon.com/vpc/
    2. Click on "Start VPC Wizard" then "Select"
    3. Fill in the "VPC name" field with your desired name (e.g. "Kevin's VPC")
    4. Select "us-west-2a" for the "Availability Zone" field
    5. Select "dedicated" for the "Hardware tenancy" field

### Launch a dedicated c3.4xlarge EC2 instance in your VPC:
    1. Open the Amazon EC2 console at https://console.aws.amazon.com/ec2/
    2. Click on "AMIs" under "IMAGES" in the "EC2 Dashboard"
    3. Search for the public PCC AMI by using filtering by AMI ID.
       The AMI ID is: ami-97fec1a7 
       The AMI Name is: CS244-SPR1415-PCC
    4. Right click on the AMI and click "Launch"
    5. Select c3.4xlarge as your instance and click on "Next: Configure Instance Details"
    6. Select your VPC in the "Network" field
    7. Choose "Enable" for the "Auto-assign Public IP" field (this allows you to ssh)
    8. Click "Review and Launch" then "Launch" using your desired key pair
    9. It should be running in your EC2 console: ssh in as normal using "ubuntu@ec2..."

### Run the following commands:
    cd cs244-final-pa
    sudo ./run.sh
    
    This will take between 30 - 60 minutes to run.

### After running, the 8 graphs can be found in:
    buffer/buffer.png
    buffer/buffer_log.png
    loss/loss.png
    loss/loss_log.png
    many/many_pcc_active.png
    many/many_pcc_inactive.png
    emulab_fig6.png
    emulab_fig7.png

Note that the PCC code is very sensitive to CPU interrupts, so please don't use the EC2 machine
at all while the demo is running. We've found that even the VM interrupts cause PCC's performance
to degrade (as described in our post) and so minimizing load on the machines would be ideal.
Note that because of this, there is a possibility you may have to run the code more than once 
(2-3 times) in order to generate similiar graphs to the report due to PCC's sensitivity to VMs and
CPU interrupts. While our graphs were generated with the "time" parameter in run.sh set to 60 sec,
which is the time spent to measure throughput, we've found that we were able to produce 
similar results (although with slightly lower frequency) with the "time" parameter set to 30 sec.
If you'd like to reduce the runtime, changing the "time" parameter may help, although with
some minor caveats. :)

For more information on how to launch a dedicated instance:
    http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/dedicated-instance.html
