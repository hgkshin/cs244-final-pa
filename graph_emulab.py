import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def graph_figures(xlabel, ylabel, title, plot_name, filename):
    f = open(filename, 'r')
    rows = []
    for row in f:
        rows.append(row.split())
    x_values = [float(x.replace("KB", "")) for x in rows[0][1:]]
    legend = []
    for experiment in rows[1:]:
        experiment_name = experiment[0]
        legend.append(experiment[0])
        y_values = [float(y) for y in experiment[1:]]
        plt.plot(x_values, y_values, '-x')
    plt.yscale('log')
    if filename == 'emulab_data/figure_6_data':
        plt.xscale('log')
    plt.legend(legend, loc='best')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.savefig(plot_name)
    plt.close()

if __name__ == '__main__':
    # Figure 6
    xlabel = 'Bottleneck Buffersize (KB)'
    ylabel = 'Average Receiving Throughput (Mbps)'
    title = 'Emulab Figure 6 Reproduced: PCC vs TCP on Emulated Satellite Links'
    plot_name = 'emulab_fig6.png'
    graph_figures(xlabel, ylabel, title, plot_name, 'emulab_data/figure_6_data')
    # Figure 7
    xlabel = 'Loss Rates'
    ylabel = 'Average Receiving Throughput (Mbps)'
    title = 'Emulab Figure 7 Reproduced: PCC vs TCP in Random Loss'
    plot_name = 'emulab_fig7.png'
    graph_figures(xlabel, ylabel, title, plot_name, 'emulab_data/figure_7_data')

