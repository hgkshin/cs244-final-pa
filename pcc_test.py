#!/usr/bin/python

"CS244 Spring 2015 Assignment 3: PCC vs TCP. The showdown."

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.util import dumpNodeConnections
from mininet.cli import CLI
import numpy

from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser
from helper import *
import matplotlib.pyplot as plt
from monitor import monitor_qlen
import termcolor as T

import sys
import os
import math
import pprint

parser = ArgumentParser(description="PCC vs TCP Tests")

# We follow various experiments as outlined in the paper. The different options are:
#   loss: measures the throughput of PCC and TCP under various loss conditions
#   buffer: measures the throughput of PCC and TCP under various bottleneck buffer sizes
#   mix: measures throughput when PCC and TCP are mixed together over time
# This also dictates which directory output our final result will be in
parser.add_argument('--exp',
                    help="which experiment to run",
                    default="loss")
parser.add_argument('--iperf',
                    dest='iperf',
                    help="Path to custom iperf",
                    required=True)
parser.add_argument('--time',
                    type=float,
                    help="Time to run each throughput measurement (s)",
                    required=True)

# Experiment types
LOSS_EXP = 'loss'
BUFFER_EXP = 'buffer'
MANY_EXP = 'many'

# Experiment parameters
args = parser.parse_args()
BWHOST = None
BWNET = None
DELAY = None
MAX_Q = None
LOSS = 0
CUSTOM_IPERF_PATH = args.iperf
assert(os.path.exists(CUSTOM_IPERF_PATH))

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def draw_graphs(protocol_names, main_dir):
    ylabel = 'Average Receiving Throughput (Mbps)'
    if args.exp == LOSS_EXP:
        xlabel = 'Loss Rates'
        title = 'Mininet Figure 7 Reproduced: PCC vs TCP in Random Loss'
    if args.exp == BUFFER_EXP:
        xlabel = 'Bottleneck Buffersize (KB)'
        title = 'Mininet Figure 6 Reproduced: PCC vs TCP on Emulated Satellite Links'
    if args.exp == MANY_EXP:
        xlabel = 'Time (Seconds)'
        ylabel = "Sending Throughput (Mbps)"
        title1 = 'Mininet Concurrent Flow Throughput for 4 TCP Flows and 1 PCC Flow'
        title2 = 'Mininet Concurrent Flow Throughput for 5 TCP Flows'

    # Used for MANY experiment
    pcc_active = {}
    pcc_inactive = {}
    # Used for LOSS/BUFFER experiment
    protocol_stats = {}

    protocol_dirs = os.listdir(main_dir)
    for protocol_dir in protocol_dirs:
        if os.path.isdir(main_dir +'/' + protocol_dir) and protocol_dir in protocol_names:
            experiment_files = os.listdir(main_dir +'/' + protocol_dir)
            for experiment_file in experiment_files:
                f = open(main_dir +'/' + protocol_dir + '/' + experiment_file, 'r')
                experiment_throughputs = []
                for line in f:
                    if len(experiment_throughputs) == args.time:
                        break
                    if protocol_dir != 'pcc' and 'Mbits/sec' in line:
                        experiment_throughputs.append(float(line.split()[-2]))
                    elif protocol_dir == 'pcc' and 'send' in experiment_file:
                        if is_number(line.split()[0]):
                            experiment_throughputs.append(float(line.split()[0]))
                    elif len(line.split()) == 1 and is_number(line.split()[0]):
                        experiment_throughputs.append(float(line.split()[0]))

                # LOSS/BUFFER
                if args.exp != MANY_EXP and 'recv' in experiment_file:
                    experiment_stats = {}
                    experiment_stats['median'] = numpy.median(experiment_throughputs)
                    experiment_stats['mean'] = numpy.mean(experiment_throughputs)
                    exp_name = experiment_file.split('_')[0]
                    if protocol_dir not in protocol_stats:
                        protocol_stats[protocol_dir] = {}
                    protocol_stats[protocol_dir][exp_name] = experiment_stats
                # MANY
                elif args.exp == MANY_EXP and 'send' in experiment_file:
                    flow_num = experiment_file
                    flow_num = flow_num.replace('pcc_active_', '')
                    flow_num = flow_num.replace('pcc_inactive_', '')
                    flow_num = flow_num.replace('_cubic_send.txt', '')
                    flow_num = flow_num.replace('_pcc_send.txt', '')
                    if 'pcc_inactive' in experiment_file:
                        pcc_inactive['cubic flow ' + flow_num] = experiment_throughputs
                    else:
                        if protocol_dir != 'pcc':
                            pcc_active['cubic_flow ' + flow_num] = experiment_throughputs
                        else:
                            pcc_active['pcc_flow'] = experiment_throughputs

    ## Plot Time
    if args.exp != MANY_EXP:
        line_names = protocol_stats.keys()
        legend = []
        for line in line_names:
            x_str = list(protocol_stats[line].keys())
            x_values = [float(x) for x in x_str]
            if args.exp == BUFFER_EXP:
                x_values = [1.5 * x for x in x_values]
            num_to_str = {}
            for i in xrange(len(x_values)):
                num_to_str[x_values[i]] = x_str[i]
            x_values = sorted(x_values)
            y_values = []
            for x in x_values:
                y_values.append(protocol_stats[line][num_to_str[x]]['mean'])
            if args.exp == LOSS_EXP:
                x_values = [float(x)/100 for x in x_values]
            plt.plot(x_values, y_values, '-x')
            legend.append(line)
        if args.exp == BUFFER_EXP:
            plt.xscale('log')
        plt.yscale('log')
        plt.legend(legend, loc='best')
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(title)
        plt.savefig(main_dir + '/' + main_dir + '_log.png')
        plt.xscale('linear')
        plt.yscale('linear')
        plt.savefig(main_dir + '/' + main_dir + '.png')
        plt.close()
    else:
        count = 0
        for stat_dict in [pcc_active, pcc_inactive]:
            legend = []
            flow_names = stat_dict.keys()
            for name in flow_names:
                y_values = stat_dict[name]
                y_values = [float(x) for x in y_values]
                plt.plot(y_values)
                legend.append(name)
            plt.legend(legend, loc='best')
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            if count == 0:
                plt.title(title1)
                plt.savefig(main_dir + '/' + main_dir + '_pcc_active.png')
            else:
                plt.title(title2)
                plt.savefig(main_dir + '/' + main_dir + '_pcc_inactive.png')
            count += 1
            plt.close()
    return

class BBTopo(Topo):
    "Simple topology for bufferbloat experiment."

    def build(self, n=2, loss=0, max_q=None):
        if max_q is None:
            max_q = MAX_Q

        # Create two hosts
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        print "BW: " + str(BWHOST)
        print "BW: " + str(BWNET)
        print "DELAY: " + str(DELAY)
        print "MAX Q: " + str(max_q)
        print "LOSS: " + str(loss)

        # Here I have created a switch.  If you change its name, its
        # interface names will change from s0-eth1 to newname-eth1.
        switch = self.addSwitch('s0')

        # Add links with appropriate characteristics
        # Server to Switch (1)
        self.addLink(h1, switch, bw=BWHOST, delay=str(DELAY) + 'ms', loss=loss)

        # Switch to Client (2)
        self.addLink(h2, switch, bw=BWNET, delay=str(DELAY) + 'ms', loss=0, max_queue_size=max_q)

        return

# Simple wrappers around monitoring utilities.  You are welcome to
# contribute neatly written (using classes) monitoring scripts for
# Mininet!
def start_qmon(iface, interval_sec=0.1, outfile="q.txt"):
    monitor = Process(target=monitor_qlen,
                      args=(iface, interval_sec, outfile))
    monitor.start()
    return monitor

def start_iperf(net, cong_type, filename_value):
    seconds = args.time + 3600 # flows guaranteed to last longer than each experiment
    h2 = net.get('h2')
    h1 = net.get('h1')
    print "Starting iperf server for " + str(cong_type) + "..."
    # For those who are curious about the -w 16m parameter, it ensures
    # that the TCP flow is not receiver window limited.  If it is,
    # there is a chance that the router buffer may not get filled up.
    # os.system("sysctl -w net.ipv4.tcp_congestion_control=%s" % cong_type)
    filename_prefix = filename_value + "_" + cong_type
    server = h2.popen('%s -s -p %s -f m -i 1 -w 16m > %s/%s/%s' % \
                      (CUSTOM_IPERF_PATH, 5001, args.exp, cong_type, \
                       filename_prefix + '_recv.txt'), \
                      shell=True)
    client = h1.popen('%s -c %s -p %s -t %d -Z %s -f m -i 1 > %s/%s/%s' % \
                      (CUSTOM_IPERF_PATH, h2.IP(), 5001, seconds, \
                       cong_type, args.exp, cong_type, filename_prefix + '_send.txt'), \
                      shell=True)

def start_pcc(net, filename_value):
    h2 = net.get('h2')
    h1 = net.get('h1')
    print "Starting pcc server..."
    server = h2.popen("/home/ubuntu/pcc/receiver/app/appserver > %s/pcc/%s_pcc_recv.txt" % \
                      (args.exp, filename_value), \
                      env=dict(os.environ, LD_LIBRARY_PATH="/home/ubuntu/pcc/receiver/src"), \
                      shell=True)
    client = h1.popen("/home/ubuntu/pcc/sender/app/appclient %s 9000 > %s/pcc/%s_pcc_send.txt" % \
                      (h2.IP(), args.exp, filename_value), \
                      env=dict(os.environ, LD_LIBRARY_PATH="/home/ubuntu/pcc/sender/src"), \
                      shell=True)

# Starts a mininet environment with the given parameters
def start_experiment(num_tcp_flows, num_pcc_flows, param_value=None, cong_type=None):
    # Work to create pcc/tcp flow directories
    pcc_dir_name = args.exp + '/pcc'
    if not os.path.exists(pcc_dir_name):
        os.makedirs(pcc_dir_name)
    if cong_type is not None:
        tcp_dir_name = args.exp + '/' + cong_type
        if not os.path.exists(tcp_dir_name):
            os.makedirs(tcp_dir_name)

    # Set up mininet topology
    loss= LOSS
    max_q = MAX_Q
    if (args.exp == LOSS_EXP):
        loss = param_value
    if (args.exp == BUFFER_EXP):
        max_q = param_value
    topo = BBTopo(loss=loss, max_q=max_q)

    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()
    dumpNodeConnections(net.hosts)
    net.pingAll()
    qmon = start_qmon(iface='s0-eth2',
                      outfile='%s/q.txt' % (args.exp))

    # Start flow(s)
    if args.exp == MANY_EXP:
        filename = 'pcc_active'
        if num_pcc_flows == 0:
            filename = 'pcc_inactive'
    else:
        filename = str(param_value)

    for i in xrange(num_tcp_flows):
        start_iperf(net, cong_type, filename + "_" + str(i))
    for i in xrange(num_pcc_flows):
        start_pcc(net, filename + "_" + str(i))
    return (net, qmon)

# Kills/cleans up all flows
def kill_experiment(net, qmon):
    print "Killing all flows..."
    qmon.terminate()
    net.stop()
    os.system('mn -c')
    os.system('killall appserver')
    os.system('killall iperf')
    os.system('killall appclient')

def loss_exp():
    congs = ['pcc', 'cubic', 'illinois']
    losses = [0, 1, 2, 3, 4, 5, 6]

    global BWNET
    global BWHOST
    global DELAY
    global MAX_Q

    BWNET = 100
    BWHOST = 100
    DELAY = 7.5
    MAX_Q = 250

    for loss in losses:
        print 'Starting experiment for loss: ' + str(loss)
        print ''
        for cong_type in congs:
            if cong_type == 'pcc':
                (net, qmon) = start_experiment(num_tcp_flows = 0, num_pcc_flows = 1,\
                                               param_value = loss)
            else:
                (net, qmon) = start_experiment(num_tcp_flows = 1, num_pcc_flows = 0, \
                                               param_value = loss, cong_type=cong_type)
            sleep(args.time)
            kill_experiment(net, qmon)
    draw_graphs(congs, LOSS_EXP)
    return

def buffer_exp():
    congs = ['cubic', 'hybla', 'illinois', 'pcc', 'reno']

    buffers = [1, 5, 50, 200, 500, 660]
    global BWNET
    global BWHOST
    global DELAY
    global LOSS

    BWNET = 42
    BWHOST = 42
    DELAY = 200
    LOSS = 1

    for buffer in buffers:
        print 'Starting experiment for qsizes: ' + str(buffer)
        print ''
        for cong_type in congs:
            if cong_type == 'pcc':
                (net, qmon) = start_experiment(num_tcp_flows = 0, num_pcc_flows = 1, \
                                               param_value=buffer)
            else:
                (net, qmon) = start_experiment(num_tcp_flows = 1, num_pcc_flows = 0, \
                                               param_value = buffer, cong_type=cong_type)
            sleep(args.time)
            kill_experiment(net, qmon)
    draw_graphs(congs, BUFFER_EXP)
    return

def many_exp():
    global BWNET
    global BWHOST
    global DELAY
    global MAX_Q
    global LOSS

    BWNET = 100
    BWHOST = 100
    DELAY = 7.5
    MAX_Q = 250
    LOSS = 0

    (net, qmon) = start_experiment(num_tcp_flows = 5, num_pcc_flows = 0, cong_type='cubic')
    sleep(args.time)
    kill_experiment(net, qmon)

    (net, qmon) = start_experiment(num_tcp_flows = 4, num_pcc_flows = 1, cong_type='cubic')
    sleep(args.time)
    kill_experiment(net, qmon)
    draw_graphs(['pcc', 'cubic'], MANY_EXP)
    return

if __name__ == "__main__":
    # Hint: The command below invokes a CLI which you can use to
    # debug.  It allows you to run arbitrary commands inside your
    # emulated hosts h1 and h2.
    # CLI(net)
    if not os.path.exists(args.exp):
        os.makedirs(args.exp)

    if (args.exp == LOSS_EXP):
        loss_exp()
    elif (args.exp == BUFFER_EXP):
        buffer_exp()
    elif (args.exp == MANY_EXP):
        many_exp()

    # Ensure that all processes you create within Mininet are killed.
    # Sometimes they require manual killing.
    Popen("pgrep -f webserver.py | xargs kill -9", shell=True).wait()
