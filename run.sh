#!/bin/bash

# Note: Mininet must be run as root.  So invoke this shell script
# using sudo.

iperf='/usr/bin/iperf'

time=60
python graph_emulab.py
for exp in 'buffer' 'loss' 'many'; do
  mn -c
  rm -rf $exp
  python pcc_test.py --exp $exp\
                     --iperf $iperf\
                     --time $time
done
